﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
 


    namespace UberEats
    {
        public interface ICuisine
        {
            double rerturnCuisineNumber { get; }
            void showCuisine();
            void addToList(ICuisineList orderList);
        }
        public interface IResturant
        {
            int getResturantNumber { get; }
            void showResturant;
            ISeeMenu seeMenu { get; }
        }

        public interface ISeeMenu
        {
            ICuisineList getMenu;
        }
        public interface ITakeOrder
        {
            void sendOrder(ICuisineList cuisineList); //this is not OOP
        }
        public interface IRestruantList : IList<IResturant>
        {
            IResturant selectResturant(int resturnantNum);
        }

        public interface ICuisineList : IList<ICuisine>
        {
            void removeCuisine(IList<int> cuisineSelectNum);
            ICuisine getCuisine (int num) { get; }
        }

        class UberEatsUserTerminal
        {
            public delegate void programHolder();
            public event programHolder holdStep;
            IRestruantList resturantList;
            ITakeOrder takeOrder;
            IResturant resturant;
            ICuisineList menu;
            ICuisineList orderList;

            public void selectResturant()
            {
                Console.WriteLine("Please select resturant you interested");
                foreach (var resturant in resturantList)
                {
                    resturant.showResturant();//show resturant details, include resturant select number, name and some introduction etc...
                }
                int resturantNum;
                resturantNum = Convert.ToInt32(Console.ReadLine());
                resturant = resturantList.selectResturant(resturantNum);
            }

            public void seeMenu()
            {
                ICuisine cuisine;

                menu = resturant.seeMenu.getMenu();
                foreach (var cuisine in menu)
                {
                    cuisine.showCuisine();//show cuisine details, include cuisine select number, name and some introduction etc...
                }
                Console.WriteLine("Please select cuisine you interested");
                int cuisineNum;
                cuisineNum = Convert.ToInt32(Console.ReadLine());
                cuisine = menu.getCuisine(cuisineNum);
                Console.WriteLine("Press 1 add cuisine to orderList");
                if (Convert.ToInt32(Console.ReadLine()) == 1)
                {
                    orderList.Add(cuisine);
                }
            }

            public void checkOrderList()
            {
                List<int> removeList;
                Console.WriteLine("Your order list are ：");
                foreach(var cuisine in orderList)
                {
                    cuisine.showCuisine();
                }
                Console.WriteLine("Press 'A' means agree your order list, other characters to remove cuisine from order list by enter cuisine number");
                if (Convert.ToChar(Console.ReadLine()) != 'A')
                {
                    Console.WriteLine("Enter the num of cuisine which you want remove,end with '0'");
                    while (1)
                    {
                        int n = Convert.ToInt32(Console.ReadLine());
                        if (n == 0)
                            break;
                        else
                            removeList.Add(n);         
                    }
                    orderList.removeCuisine(removeList);

                }
            }

            public void RunProgram()
            {
                int numToChooseStep;
                holdStep = this.selectResturant();
                while (1)
                {
                    holdStep();
                        Console.Writeline("Press 1 for reselecting resturant, 2 for selecting other cuisine,3 for checking order list,4 for sending order ");
                        numToChooseStep = Convert.ToInt32(Console.ReadLine);
                    if (numToChooseStep== 1)
                        holdStep = this.selectResturant();
                    if (numToChooseStep == 2)
                        holdStep = this.seeMenu;
                    if (numToChooseStep == 3)
                        holdStep = this.checkOrderList;
                    if (numToChooseStep == 4)
                        break;
                }
                takeOrder.sendOrder(orderList);
            }

        }
    }

}
