﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apple;
using Juice;
//Smashing apple to juice,
namespace Juicer
{
    public interface IJuicer
    {
        IApple apple { get; set; }
        IJuice Juice { get; }
        IJuice pour();
        void smashing();

    }

    
}
