﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Juice;

namespace Callback
{
    public interface ICallback
    {
        void Callback(IJuice ACupOfJuice);
    }
}
