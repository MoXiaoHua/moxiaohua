﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fruit;

namespace SpecialApple
{
    class RicherApple:IApple
    {
        private static Random rd = new Random();
        private double weight;
        public RicherApple() {
           
            weight = rd.Next(50, 200);
        }//模拟每个苹果重量不同
     
        public double Weight
        {
            set
            {
                weight = value;
            }
            get
            {
                return weight;
            }
        }
    }
}
