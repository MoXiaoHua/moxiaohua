﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit
{
    public interface IApple
    {
        double Weight { set; get; }
    }
}
