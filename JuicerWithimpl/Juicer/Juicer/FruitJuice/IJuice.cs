﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juice
{
    public interface IJuice
    {
        void drink();
        double Weight { get; set; }
    }
}
