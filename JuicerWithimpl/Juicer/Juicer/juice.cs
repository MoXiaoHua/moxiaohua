﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Juice;

namespace specialJuice
{
    class SpecialJuice:IJuice
    {
        double theWeight;
        public double Weight
        {
            get
            {
                return this.theWeight;
            }
            set
            {
                this.theWeight =value;
            }
        }
      
        public SpecialJuice(double weight=0)
        {
            this.theWeight = weight;
        }
       
        public void drink()
        {

            if (this.theWeight == 0)
            {
                Console.WriteLine("果汁还没榨好，请等待一下哟");
                return;
            }    
            Console.WriteLine("{0} 毫升被果汁喝完了",this.Weight);
            this.Weight = 0;
        }
    }
}
