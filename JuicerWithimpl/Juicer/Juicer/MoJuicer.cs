﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Juicer;
using Juice;
using Fruit;
using specialJuice;
using SpecialApple;
using System.Threading;
namespace MoJuicers
{
    
    class MoJuicer:IAppleJuicer
    {
        private Queue<IClient> clients=new Queue<IClient>();
        public event Action<IJuice> juiceReady;
        private Queue<IJuice> juicePlate=new Queue<IJuice>();
        private AutoResetEvent myEvent = new AutoResetEvent(false);
        private  Thread backThread;
        private IApple apple;
        public AutoResetEvent resetEvent
        {
            get
            {
                return myEvent;
            }
        }
        public IApple Apple
        {
            set
            {
                apple = value;
            }
        }//每次榨汁前  需要放入一个苹果；
       private IJuice juice=new SpecialJuice();
       public IJuice Juice
        {
            get
            {
                return PourTheJuice();
            }
        }
       public void smashing()
        {
            backThread = new Thread(run);
            backThread.Start();
          //  this.resetEvent.WaitOne();
        }
        public IJuice PourTheJuice()
        {
            return juicePlate.Dequeue();
        }
        public void JuiceDone(IJuice cupOfJuice)
 
       {
            Console.WriteLine("这一杯果汁有 {0} 毫升",cupOfJuice.Weight);
            Console.WriteLine("果汁已经好啦。尽快喝完哟");
            IJuice theJuice = new SpecialJuice(juice.Weight);
            juicePlate.Enqueue(theJuice);
           // this.TellClientJuiceIsDone();
            juice.Weight = 0;
            IClient clientThis = clients.Dequeue();
            juiceReady += clientThis.DrinkTheJuice;
            juiceReady(this.Juice);
            juiceReady -= clientThis.DrinkTheJuice;
        }

    
        public void run()
        {
            double i=0;
            //lock (this)
           // {
                this.Apple = new RicherApple();
                double weight = this.apple.Weight;
                while(true)
                {
                    Thread.Sleep(1000);
                if (i < weight)
                {
                    Console.WriteLine("正在榨汁中，进度 {0} %", i / weight * 100);
                    juice.Weight = WeightLoss(i);
                    this.apple.Weight -= 5 / weight * 100;
                }
                if (i >= weight)
                {
                    Console.WriteLine("正在榨汁中，进度 {0} %",  100);
                    juice.Weight = WeightLoss(weight);
                    this.apple.Weight = 0;
                    break;
                }
                    
                    i = i + 5;
                }
                this.JuiceDone(this.juice);
           // }
        }
        public void TellClientJuiceIsDone()
        {
            this.resetEvent.Set();
        }
        public double WeightLoss(double w)
        {
            return w * 3 / 4;
        }

        public void recordClient(IClient client)
        {
            clients.Enqueue(client);
        }
        
    }
}
