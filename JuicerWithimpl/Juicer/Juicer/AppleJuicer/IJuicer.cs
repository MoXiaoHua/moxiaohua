﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fruit;
using System.Threading;
using Juice;
//Smashing apple to juice,
namespace Juicer
{
    public interface IAppleJuicer
    {
        void recordClient(IClient client);
        IJuice Juice { get;}
        IApple Apple { set; }
        event Action<IJuice> juiceReady;
        void smashing();

    }

    
}
