﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Juice;
using System.Threading;
namespace Juicer
{
    public interface IClient
    {
        void DrinkTheJuice(IJuice juice);
        void BuyCupOfJuice();

        IAppleJuicer IAppleJuicer { get; set; }
    }
}
